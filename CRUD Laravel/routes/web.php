<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@home');

Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@kirim');


Route::get('/master', function () {
    return view('layout.master');
});

Route::get('/data-tables', function () {
    return view('halaman.data-table');
});

Route::get('/table', function () {
    return view('halaman.table');
});


//CRUD Cast
//Menampilkan data Cast
Route::get('/cast', 'CastController@index');

//Form Input data Cast
Route::get('/cast/create', 'CastController@create');

//Menyimpan data Cast
Route::post('/cast', 'CastController@store');

//Menampilkan detail data Cast
Route::get('/cast/{cast_id}', 'CastController@show');

//Update data Cast
//Form Edit Data Cast
Route::get('/cast/{cast_id}/edit', 'CastController@edit');

//Route untuk Update Data Cast
Route::put('/cast/{cast_id}', 'CastController@update');

//Route untuk Delete Data Cast
Route::delete('/cast/{cast_id}', 'CastController@destroy');

